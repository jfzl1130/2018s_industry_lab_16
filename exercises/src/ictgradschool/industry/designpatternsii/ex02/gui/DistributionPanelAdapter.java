package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class DistributionPanelAdapter implements CourseListener {

	private Course course;

	private DistributionPanel distributionPanel;

	public DistributionPanelAdapter(DistributionPanel distributionPanel) {
	this.distributionPanel= distributionPanel;
	}



	@Override
	public void courseHasChanged(Course course) {
		distributionPanel.repaint();


	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
}
