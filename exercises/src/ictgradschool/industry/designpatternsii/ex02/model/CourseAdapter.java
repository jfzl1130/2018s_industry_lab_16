package ictgradschool.industry.designpatternsii.ex02.model;


import ictgradschool.industry.designpatternsii.ex02.gui.StatisticsPanel;

import javax.swing.table.AbstractTableModel;
import java.util.IdentityHashMap;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class CourseAdapter extends AbstractTableModel implements CourseListener { // extends and implements

	private Course course;


	public  CourseAdapter(Course course) {
		this.course= course;

	}


	@Override
	public int getRowCount() {
		return course.size();
	}

	@Override
	public int getColumnCount() {
		return 7;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex ){
			case 0: return course.getResultAt(rowIndex)._studentID;
			case 1: return course.getResultAt(rowIndex)._studentSurname;
			case 2: return course.getResultAt(rowIndex)._studentForename;
			case 3: return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Exam);  // law: method in Student Result class
			case 4: return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Test);
			case 5: return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Assignment);
			case 6: return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Overall);

		}

		return null;

	}

	@Override
	public void courseHasChanged(Course course) {
		fireTableDataChanged();  //law: override the listener, updates the table data.

	}



	@Override
	public String getColumnName(int column) {  //law: the name is manually typed base on the table name.

		switch (column) {
			case 0:
				return "StudentID";
			case 1:
				return "Surname";
			case 2:
				return "Forename";
			case 3:
				return "Exam";
			case 4:
				return "Test";
			case 5:
				return "Assignment";
			case 6:
				return "Overall";
		}

		return null;

	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
}